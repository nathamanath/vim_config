#!/bin/bash

# Set up vim config

SRC_DIR="$(pwd)"

cd $HOME

ln -sf $SRC_DIR .vim
ln -sf $SRC_DIR/.vimrc .vimrc

cd $SRC_DIR

git submodule init
git submodule update
