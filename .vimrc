" Some taken from thoughtbot/dotfiles
" And also amix/vimrc

execute pathogen#infect()

filetype plugin on
filetype indent on " required!
syntax on

" Session management... goes with obsession.vim
" Create a session, then obsession keeps it up to date for you.
fu! RestoreSession()
  " only if no args passed to vim
  if argc() ==0
    " only if session already exists
    if filereadable(getcwd() . '/Session.vim')
      " this sets obsession off
      execute 'source ' . getcwd() . '/Session.vim'
    endif
  endif
endfunction

autocmd VimEnter * nested call RestoreSession()

source ~/.vim/partials/visual_at.vim

" automatically rebalance splits on vim resize
autocmd VimResized * :wincmd =

" zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>- :wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>= :wincmd =<cr>

":W to suo save filesj
command! W w !sudo tee % > /dev/null

" Easy gitting
cnoreabbrev G Git
cnoreabbrev g Git

" Treat <li> and <p> tags like the block tags they are
let g:html_indent_tags = 'li\|p'

" vim grep
let Grep_Skip_Dirs = '.git _build dist node_modules deps'

" allows for more combos...
let mapleader = '\'

" Filetype speciffic settings {{{
augroup vimrcEx
  autocmd!

  " Set syntax highlighting for specific file types
  autocmd BufRead,BufNewFile Appraisals set filetype=ruby
  autocmd BufRead,BufNewFile *.md set filetype=markdown
  autocmd BufRead,BufNewFile evo* set filetype=mail

  " For all text files set 'textwidth' to 80 characters.
  autocmd FileType mail setlocal textwidth=80
  autocmd FileType markdown setlocal textwidth=80
  autocmd FileType text setlocal textwidth=80

  autocmd FileType gitcommit setlocal spell
  autocmd FileType mail setlocal spell
  autocmd FileType markdown setlocal spell
  autocmd FileType text setlocal spell

  " When editing a file, always jump to the last known cursor position.
  " Don't do it for commit messages, when the position is invalid, or when
  " inside an event handler (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if &ft != 'gitcommit' && line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

  autocmd FileType vue syntax sync fromstart
augroup END

" Use formatters on save for set file types
augroup js
  autocmd!

  autocmd BufWritePost *.js silent! exec '!./node_modules/.bin/prettier --write' shellescape(@%, 1)
  autocmd BufWritePost *.vue silent! exec '!./node_modules/.bin/prettier --write' shellescape(@%, 1)
  autocmd BufWritePost *.html silent! exec '!./node_modules/.bin/prettier --write' shellescape(@%, 1)
  autocmd BufWritePost *.rb silent! exec '!./node_modules/.bin/prettier --write' shellescape(@%, 1)
  autocmd BufWritePost *.erb silent! exec '!./node_modules/.bin/prettier --write' shellescape(@%, 1)
  autocmd BufWritePost *.json silent! exec '!./node_modules/.bin/prettier --write' shellescape(@%, 1)
augroup END

augroup rs
  autocmd!

  autocmd BufWritePost *.rs silent! exec '!rustfmt' shellescape(@%, 1)
augroup END

augroup ru
  autocmd!

  autocmd BufWritePost *.rb silent! exec '!rufo' shellescape(@%, 1)
  autocmd BufWritePost *.rake silent! exec '!rufo' shellescape(@%, 1)
augroup END

augroup elixir
  autocmd!

  autocmd BufWritePost *.exs silent! exec '!mix format' shellescape(@%, 1)
  autocmd BufWritePost *.ex silent! exec '!mix format' shellescape(@%, 1)

  autocmd BufWritePost *.exs edit
  autocmd BufWritePost *.exs redraw!
augroup END
" }}}

" Vimscript file settings {{{
augroup filetype_vim
  autocmd!

  autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}

" Editor settings {{{
" be iMproved
set nocompatible

" allow omnicompletion form language addon for current filetype
" ctrl-x ctrl-o
set omnifunc=syntaxcomplete#Complete

" Auto read when a file is changed
set autoread

" have this many lines between cursor and edge of window when you can
set so=10

"No swapfiles... we use git for this"
set nowritebackup
set nobackup
set noswapfile

" highlight current line and column
highlight CursorColumn ctermbg=239
highlight CursorLine ctermbg=239

:nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>

" Backspace act as intended
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" nicer handling of case when searching
set ignorecase
set smartcase

" highlight search results
set hlsearch

" nicer search
set incsearch

" dont redraw during macro execution
" set lazyredraw

" for regexes... makes them work like grep
set magic

" show matching brackets
set showmatch
set mat=2

" dont make that annoying noise on errors!!
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" timeout
set tm=600

" Remember this many changes
set history=50

" display incomplete commands
set showcmd

" do incremental searching
set incsearch

" Statusline
" Always display the status line
set laststatus=2

" Automatically :write before running commands
set autowrite

" path to tags file... created via git hooks. see git_template
set tags=./tags,tags

" Display trailing  white spaces
set list listchars=tab:»·,trail:·

" Show line numbers
set number
set numberwidth=4
set relativenumber

"Command bar height
set cmdheight=3

set lbr
set tw=500
set wrap

" Colorschemes
set t_Co=256
set background=dark
colorscheme solarized

set encoding=utf8
set ffs=unix,dos,mac

" Room to show n levels of folding in gutter
set foldcolumn=2

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" line length marker
set colorcolumn=80

" Tab completion
" will insert tab at beginning of line,
" will use completion if not at beginning
set wildmode=list:longest,list:full
set wildmenu
set complete=.,w,b,u,t,i,kspell
" }}}

" Nice cursor {{{
if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif
" }}}

" Tabs and spaces {{{
""""""""
" tabs "
""""""""
" tabs are 2 spaces by default
set tabstop=2
set shiftwidth=2

"soft tabs"
set expandtab
set smarttab
set autoindent
set smartindent

augroup spacing
  autocmd!

  " some filetypes are fussy about tab style
  autocmd FileType make set noexpandtab shiftwidth=4 softtabstop=0
  autocmd FileType snippets set noexpandtab shiftwidth=2 softtabstop=0

  " Clear trailing white space above on every save
  autocmd BufWritePre * if &ft!="snippets"|:call <SID>StripTrailingWhitespaces()|endif

  " Retab on load for whitelisted file types
  " autocmd BufReadPost * if &modifiable | retab | endif
augroup END
" }}}

" Helper functrions {{{

" Remove trailing white space
function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

function! HasPaste()
  if &paste
    return 'PASTE MODE  '
  endif
  return ''
endfunction

function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-p>"
    endif
endfunction
" }}}

" Editor mappings {{{
" learn to use hjkl
nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Left> <nop>
nnoremap <Right> <nop>

" learn not to use backspace and delete in insert mode
:inoremap <BS> <Nop>
:inoremap <Del> <Nop>

" easy folding
nnoremap <Space> za

" ctrlP
" ctags integration
nnoremap <leader>. :CtrlPTag<cr>

" amnesia
nnoremap <leader>al :AmnShareLine<CR>
nnoremap <leader>af :AmnShareLines<CR>

" vim-test mappings
nnoremap <silent> <leader>tn :TestNearest<CR>
nnoremap <silent> <leader>tf :TestFile<CR>
nnoremap <silent> <leader>ts :TestSuite<CR>
nnoremap <silent> <leader>tl :TestLast<CR>
nnoremap <silent> <leader>tg :TestVisit<CR>

let test#strategy = {
  \ 'nearest': 'vimterminal',
  \ 'file':    'vimterminal',
  \ 'suite':   'basic',
\}

" fast save
nnoremap <leader>w :w!<cr>

inoremap <Tab> <c-r>=InsertTabWrapper()<cr>

" Switch between the last two files
" nnoremap <leader><leader> <c-^>

" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

"""""""""""
" Buffers "
"""""""""""
nnoremap <leader>l :bnext<cr>
nnoremap <leader>h :bprevious<cr>

""""""""
" Misc "
""""""""
"toggle spellchecking"
nnoremap <leader>ss :setlocal spell!<cr>

"toggle paste mode"
nnoremap <leader>pp :setlocal paste!<cr>

" Quickie buffers for scratchpad
nnoremap <leader>q :e ~/buffer<cr>
nnoremap <leader>x :e ~/buffer.md<cr>
" }}}

" ctrlP {{{
" f5 to clear cache
let g:ctrlp_use_caching = 1
let g:ctrlp_working_path_mode = 0
let g:ctrlp_max_height = 20
let g:ctrlp_custom_ignore = '\v[\/](node_modules|.git|_build|dist|deps|tmp|bin|log)$'
let g:ctrlp_show_hidden = 1
" }}}

" snipmate {{{
ino <c-j> <c-r>=snipMate#TriggerSnippet()<cr>
snor <c-j> <esc>i<right><c-r>=snipMate#TriggerSnippet()<cr>
" }}}

" lightline {{{
set noshowmode

let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ 'active': {
      \   'left': [ ['mode', 'paste'],
      \             ['fugitive', 'readonly', 'filename', 'modified'] ],
      \   'right': [ [ 'lineinfo' ], ['percent'] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&filetype=="help"?"":&readonly?"🔒":""}',
      \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
      \   'fugitive': '%{exists("*FugitiveHead")?FugitiveHead():""}'
      \ },
      \ 'component_visible_condition': {
      \   'readonly': '(&filetype!="help"&& &readonly)',
      \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
      \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
      \ },
      \ 'separator': { 'left': ' ', 'right': ' ' },
      \ 'subseparator': { 'left': ' ', 'right': ' ' }
      \ }
" }}}

" emmet {{{
let g:user_emmet_install_global = 0
autocmd FileType html,css,vue,erb,eruby,eex EmmetInstall
" }}}

" Fugitive {{{
set diffopt+=vertical
" }}}
